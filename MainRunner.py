# Title        : Main Runner
# Purpose      : This consists the maim method where all the implementations shall be run from
# Author       : Shanmugarajah Prasanna (SHPRLK)
# Limitation   : This will be continuously altered without any clear direction of development

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200511      SHPRLK              Tested Two Dimensional negative numbers.
# 20200510      SHPRLK              Tested Rook attack.
# 20200509      SHPRLK              Tested StringNumberComparison.
# 20200509      SHPRLK              Tested Second largest number.
# 20200507      SHPRLK              Added file and tested Dictionary based addition pair.
# ----------------------------------------------------------------------------------------------------------------------
from LinkedInInterviewPreperation.TwoDimNegativeNumber      import TwoDimNegNumbers
from LinkedInInterviewPreperation.RookAttack                import RookAttack
from LinkedInInterviewPreperation.StringNumberComparison    import StringNumberComparison
from LinkedInInterviewPreperation.SecondLargestNumber       import  SecondLargestNumber
from LinkedInInterviewPreperation.DictionaryPair            import DictionaryPairAddition



class MainRunner:
    def main():
        twoDimArray = [[-10,-2,-1,9],
                        [-4,0,2,5],
                        [-3,2,6,9],
                        [-1,32,9,1]]
        tdnn = TwoDimNegNumbers(twoDimArray)
        print(tdnn.countNegativeNumbers())
        print(tdnn.countNegativeNumLinearTime())

        ##----------------Rook Attack ----------------------------------
        # ra = RookAttack(3, [[0,0,1],
        #                     [0,0,0],
        #                     [0,0,1]])
        # if ra.isRooksSafe():
        #     print("All rooks are safe")
        # else :
        #     print ("It's not looking good, they are going to kill each other")

        # # -------------String Number Comparison----------------------
        # scn = StringNumberComparison("09000000", "0100")
        # if scn.isFirstDigitGreater():
        #     print ("first Digit is bigger")
        # else:
        #     print ("Actually second digit is bigger")

        ## ----------------Second Largest Number------------------------
        # sln = SecondLargestNumber([621,50,6,1,622,590,4,512,26,67])
        # print(sln.findSecondMax())

        ## -----------------Dictionay Pair to get Addition Value--------
        # dpa = DictionaryPairAddition([2,3,5,10,6,7,83,4,8],12)
        # print(dpa.findAdditionPair())

if __name__ == "__main__":
    MainRunner.main()