# Title        : Rook Attack
# Purpose      : This consists logic to say if the Rooks are safely placed in a given board
# Author       : Shanmugarajah Prasanna (SHPRLK)
# Limitation   : Does not handle values other than integers

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200510      SHPRLK              Added file and Implemented Rook attach to return true if they are safe.
# ----------------------------------------------------------------------------------------------------------------------

class RookAttack:
    boardSizeSingleSide = 0
    boardRookLayout =[]
    def __init__(self,boardSize, boardLayout):
        self.boardRookLayout        = boardLayout
        self.boardSizeSingleSide    = boardSize


    def isRooksSafe(self):
        rookRowValues    =[]
        rookColumnValues =[]
        rookSafety       = True
        for row_ in range(self.boardSizeSingleSide):
            for column_ in range(self.boardSizeSingleSide):
                if(self.boardRookLayout[row_][column_] == 1 ):
                    if((rookRowValues.count(row_) > 0) or (rookColumnValues.count(column_)>0)):
                        rookSafety = False
                    else:
                        rookRowValues.append(row_)
                        rookColumnValues.append(column_)
        return rookSafety
