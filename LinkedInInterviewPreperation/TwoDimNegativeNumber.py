# Title        : Two Dimensional Negative number count
# Purpose      : This consists logic to negative numbers in 2D array where numbers are sorted left to right & top to bottom
# Author       : Shanmugarajah Prasanna (SHPRLK)
# Limitation   : Does not handle values other than integers

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200511      SHPRLK              Implemented Two Dimensional Negative Number count and less time complex solution.
# 20200510      SHPRLK              Added file.
# ----------------------------------------------------------------------------------------------------------------------

class TwoDimNegNumbers:
    twoDimensionalArray = []
    def __init__(self, twoDimArray):
        self.twoDimensionalArray = twoDimArray

    def countNegativeNumbers(self):
        negativeCount = 0
        rowSize = len(self.twoDimensionalArray)
        for row_ in range(rowSize):
            for column_ in range(rowSize):
                if(self.twoDimensionalArray[row_][column_] < 0):
                    negativeCount = negativeCount + 1
                else:
                    break
        return  negativeCount

    def countNegativeNumLinearTime(self):
        negativeCount = 0
        rowSize = len(self.twoDimensionalArray)
        rowVal = 0
        colVal = (rowSize-1)
        while rowVal < rowSize and (colVal >= 0):
            if ( self.twoDimensionalArray[rowVal][colVal] < 0 ):
                negativeCount = negativeCount+ colVal+1
                rowVal = rowVal + 1
            else:
                colVal = colVal - 1

        return  negativeCount