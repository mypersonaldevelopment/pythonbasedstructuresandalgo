# Title        : Dictionary Pair
# Purpose      : This consists logic to identify the two pairs that would provide the required addition
# Author       : Shanmugarajah Prasanna (SHPRLK)
# Limitation   : Does not handle values other than integers

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200507      SHPRLK              Added file and Implemented Dictionary based addition pair.
# ----------------------------------------------------------------------------------------------------------------------

class DictionaryPairAddition:
    dictValues = {}
    finalAddition = 0;
    def __init__(self, dictForPair ,finalAddition):
        self.dictValues = dictForPair
        self.finalAddition = finalAddition


    def findAdditionPair(self):
        print(self.dictValues)
        resultDict= {}
        tempDict = {}
        for key in self.dictValues:
            if (self.finalAddition - key) in self.dictValues:
                resultDict[key]=1
                resultDict[self.finalAddition-key]=1
                return resultDict
            else:
                tempDict[key]=1
