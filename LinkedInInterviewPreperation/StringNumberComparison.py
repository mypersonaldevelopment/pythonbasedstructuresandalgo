# Title        : String Number Comparison
# Purpose      : This consists logic to compare two string numbers and if the first is large return true if not false
# Author       : Shanmugarajah Prasanna (SHPRLK)

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200509      SHPRLK              Added file and Implemented String Number Comparison
# ----------------------------------------------------------------------------------------------------------------------

class StringNumberComparison:
    firstValue   = ''
    secondValue  = ''
    length       = 0

    def __init__(self, firstNo, secondNo):
        self.firstValue  = firstNo
        self.secondValue = secondNo

    def isFirstDigitGreater(self):
        if(len(self.firstValue) > len(self.secondValue)):
            self.length = len(self.firstValue)
            self.secondValue = self.secondValue.zfill(self.length)
        else:
            self.length = len(self.secondValue)
            self.firstValue  = self.firstValue.zfill(self.length)

        for range_ in range(self.length):
            if((self.firstValue[range_]) > (self.secondValue[range_])):
                return True
            elif((self.firstValue[range_]) < (self.secondValue[range_])):
                return False
