# Title        : Second Largest Number
# Purpose      : This consists logic to identify the second largest number in a given list
# Author       : Shanmugarajah Prasanna (SHPRLK)
# Limitation   : Does not handle values other than integers

# DATE         CONTRIBUTOR ID       COMMENT
# ---------     --------------      ------------------------------------------------------------------------------------
# 20200509      SHPRLK              Added file and Implemented Second largest number
# ----------------------------------------------------------------------------------------------------------------------
import sys

class SecondLargestNumber:
    maxVal = -sys.maxsize
    secondMax =  -sys.maxsize
    listToAnalyze = []

    def __init__( self, inputList ):
        self.listToAnalyze = inputList

    def findSecondMax( self ):
        if( len( self.listToAnalyze)< 2 ):
            self.secondMax = None
        else:
            for value_ in self.listToAnalyze:
                if( self.maxVal < value_):
                    self.secondMax  = self.maxVal
                    self.maxVal     = value_
                elif (( value_ < self.maxVal ) and ( value_ > self.secondMax )):
                    self.secondMax  = value_

        return  self.secondMax